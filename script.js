/*--------- variaveis -------*/
const tabela = document.getElementById("tb_fatura");

const mesesDoAno = {
    1: 'Janeiro',
    2: 'Fevereiro',
    3: 'Março',
    4: 'Abril',
    5: 'Maio',
    6: 'Junho',
    7: 'Julho', 
    8: 'Agosto',
    9: 'Setembro',
    10: 'Outubro',
    11: 'Novembro',
    12: 'Dezembro',
}

/*------- helpers -------*/
const retornarMes = num => mesesDoAno[num];

const tabelaParaJson = (tabela) => {
    let data = [];
    let tituloHeaders = ['lancamento', 'valor', 'mes'];

    for (let i = 1; i < tabela.rows.length; i++) {
        let tabelaRow = tabela.rows[i];
        let rowData = {};
        for (let j = 1; j < tabelaRow.cells.length; j++) {
            rowData[ tituloHeaders[j] ] = parseFloat((tabelaRow.cells[j].innerHTML
                                                                 .replace(/[R$. ]+/g, ""))
                                                                 .replace(",","."));
        }
        data.push(rowData);
    }       
    return data;
}

const dataObj = tabelaParaJson(tabela);

const agrupaPorMes = () => {
    let objFinal = {};

    dataObj.forEach(obj => {
        objFinal = {
            ...objFinal,
            [obj.mes]: ((objFinal[obj.mes] === undefined ? 0 : objFinal[obj.mes]) + obj.valor)
        }
    })

    return objFinal;
}

const objAgrupadoPorMes = agrupaPorMes();

/*------- Função de criação da tabela ------*/
const criarTabelaConsolidado = () => {
    const tabelaConsolidado = document.getElementById('tb_consolidado');
    const tabelaConsolidadoHeader = tabelaConsolidado.getElementsByTagName('thead');
    const tabelaConsolidadoBody = tabelaConsolidado.getElementsByTagName('tbody');

    const linhaHeader = document.createElement('tr');
    const colunaMes = document.createElement('th');
    const colunaGasto = document.createElement('th');

    colunaMes.textContent = "Mês";
    colunaGasto.textContent = "Gasto Total";

    linhaHeader.appendChild(colunaMes);
    linhaHeader.appendChild(colunaGasto);

    tabelaConsolidadoHeader[0].appendChild(linhaHeader);

    for (const prop in objAgrupadoPorMes) {
        
        const linhaMesGasto = document.createElement('tr');
        const colunaMes = document.createElement('td');
        const colunaGasto = document.createElement('td');

        colunaMes.textContent = retornarMes(prop);
        colunaGasto.textContent = 'R$ ' + objAgrupadoPorMes[prop].toFixed(2);

        linhaMesGasto.appendChild(colunaMes);
        linhaMesGasto.appendChild(colunaGasto);
    
        tabelaConsolidadoBody[0].appendChild(linhaMesGasto);
    }
}

criarTabelaConsolidado()