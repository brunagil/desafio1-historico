# Histórico de Fatura - Lógica Javascript

### Cenário Proposto

_Neste projeto temos uma página estática em html, que mostra o histórico de lançamentos cartão de um cliente, de forma desorganizada._
_A necessidade do nosso projeto é criar um consolidado de gastos por mês que fique facilmente visível para o usuário nesta página._

### Requisitos
- _Todos os requisitos foram implementados conforme instruções_

### Para acessar o projeto
- Clone o projeto ou faça download e abra o documento `historico-fatura.html` no seu navegador de preferência

### Lógica de implementação
- Considerei um cenário onde a única maneira de conseguir as informações do histórico de lançamentos fosse através da tabela já preenchida
- Código está organizado para ser dividido em módulos

### Acesso ao [DESAFIO 2](https://gitlab.com/brunagil/desafio2-framework)! 

Obrigada pela oportunidade de participar desse processo! :) 